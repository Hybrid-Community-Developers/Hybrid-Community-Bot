package com.maks1mka.commands.moderation

import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.BaseEmbedBuilder
import com.maks1mka.utils.checkPerms
import com.maks1mka.utils.classException
import com.maks1mka.utils.fetchMember
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.time.Duration

class MuteCommand: BaseCommand() {
    override val name: String = "Mute"
    override val aliases: List<String> = listOf("mute", "ontimeout", "timeout", "addtimeout")
    override val desc: String = "Мьют участника сервера"
    override val category: String = "Модерация"
    override val usage: String = "<member> [reason|duration]"
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if (checkPerms(e, e.member!!.permissions)) return

        val member: Member? = fetchMember(e, args)

        if (classException(e, member!!.javaClass, "MemberImpl", true)) return

        if (member == e.member) {
            e.channel.sendMessage("Ты не можешь применять эту команду на себе").queue()
            return
        }

        var reason = "Причина не указана"
        var duration: Duration = Duration.ofMinutes(100)
        if(args.size == 2)
           {
               try {
                   if(args[1].toInt().javaClass.simpleName == duration.toMinutes().javaClass.simpleName) {
                       duration = Duration.ofMinutes(args[1].toLong())
                   }
               } catch (_: Exception) {
                   if(args[1].javaClass.simpleName == reason.javaClass.simpleName) {
                       reason = args.subList(1, args.size).joinToString(" ")
                   }
               }
           }
        if(args.size > 2) {
            reason = args.subList(1, args.size - 1).joinToString(" ")
            duration = Duration.ofMinutes(args[args.lastIndex].toLong())
        }
        println("${member.user.name} - $reason - ${duration.toMinutes()} min")

        val embedBuilder: EmbedBuilder = BaseEmbedBuilder().create(
            title = "Успешно!",
            desc =  "${member.asMention} замьючен\n **Причина: **$reason**\nДлительность мута: **${duration.toMinutes()} минут",
            color = e.author.retrieveProfile().complete().accentColor,
            textFooter = "Замутил ${e.author.name}",
            iconFooter = e.author.effectiveAvatarUrl
        )

        e.guild.timeoutFor(member, duration).queue().apply {
            e.channel.sendMessageEmbeds(embedBuilder.build()).queue()
        }
        return
    }
}