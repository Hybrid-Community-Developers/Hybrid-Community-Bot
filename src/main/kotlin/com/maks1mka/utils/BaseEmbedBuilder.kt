package com.maks1mka.utils

import net.dv8tion.jda.api.EmbedBuilder
import java.awt.Color

class BaseEmbedBuilder() {

    fun create(title: String? = null, desc: String? = null, color: Color? = null, textFooter: String? = null, iconFooter: String? = null): EmbedBuilder {
        val embedBuilder = EmbedBuilder()
        with(embedBuilder) {
           if(!title.isNullOrEmpty()) setTitle(title)
           if(!desc.isNullOrEmpty()) setDescription(desc)
           if(color != null) setColor(color)
           if(!textFooter.isNullOrEmpty() && !iconFooter.isNullOrEmpty()) setFooter(textFooter, iconFooter)
           if(!textFooter.isNullOrEmpty() && iconFooter.isNullOrEmpty()) setFooter(textFooter)
        }
        return embedBuilder

    }
}