package com.maks1mka.utils

import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.util.*


fun getGuildForEmoji(): String {
    return "900129288366481439"
}

fun getTimestamp(milliseconds: Long): String {
    val seconds: Int = ((milliseconds / 1000) % 60).toInt()
    val minutes: Int = (((milliseconds / (1000 * 60)) % 60)).toInt()
    val hours: Int   = (((milliseconds / (1000 * 60 * 60)) % 24)).toInt()

    return if (hours > 0)
        String.format("%02d:%02d:%02d", hours, minutes, seconds)
    else
        String.format("%02d:%02d", minutes, seconds)
}

fun fetchMember(event: MessageReceivedEvent, args: List<String>): Member? {
    return if(event.message.mentions.members.isEmpty())
        if (args.isNotEmpty()) event.guild.getMemberById(args[0])
        else event.message.member
    else event.message.mentions.members[0]
}

fun fetchUser(event: MessageReceivedEvent, args: List<String>): User {
    return if (event.message.mentions.users.size == 0)
        if (args.isNotEmpty()) event.jda.retrieveUserById(args[0]).complete()
        else event.message.author
    else event.message.mentions.users[0]
}


fun classException(event: MessageReceivedEvent, clazz: Class<Any>, name: String, message: Boolean): Boolean {
    return  if(clazz.simpleName != name) {
        if(message) event.channel.sendMessage("Произошла какая-то ошибка! Ты точно нигде не ошибся, дурашка?").queue()
        true
    }
    else false
}

fun checkPerms(event: MessageReceivedEvent, perms: EnumSet<Permission>): Boolean {
    val permsList: List<Permission> = listOf(
        Permission.ADMINISTRATOR,
        Permission.KICK_MEMBERS,
        Permission.MODERATE_MEMBERS,
        Permission.BAN_MEMBERS,
    )
    permsList.forEach { if(perms.contains(it)) return false }
    event.channel.sendMessage("У тебя нет прав на эту команду!").queue()
    return true
}
//fun timeStampParser(timeStamp: String): Duration {
//    return  Duration.ZERO
//    return Duration.parse(timeStamp)
//}