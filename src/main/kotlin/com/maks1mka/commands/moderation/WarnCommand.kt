package com.maks1mka.commands.moderation

import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.*
import com.maks1mka.utils.mongodb.UsersCollection
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.message.MessageReceivedEvent


class WarnCommand: BaseCommand() {
    override val name: String = "Warn"
    override val aliases: List<String> = listOf("warn", "addwarn")
    override val desc: String = "Предупредение участнику сервера"
    override val category: String = "Модерация"
    override val usage: String = "<member> [reason]"
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if (checkPerms(e, e.member!!.permissions)) return

        val member: Member? = fetchMember(e, args)
        if (classException(e, member!!.javaClass, "MemberImpl", true)) return

        if (member == e.member) {
            e.channel.sendMessage("Ты не можешь применять эту команду на себе").queue()
            return
        }

        val warns = UsersCollection(CLIENT, DATABASE).updateUser(e.guild.id, member.id, "\$inc", "warns", 1)!!.getInteger("warns")


        val embedBuilder: EmbedBuilder = BaseEmbedBuilder().create(
            title = "Получил предпреждение ${member.user.name}",
            desc = "**Количество полученных предпреждений:** $warns",
            color = e.author.retrieveProfile().complete().accentColor,
            textFooter = "Выдал предупреждение ${e.author.name}",
            iconFooter = e.author.effectiveAvatarUrl
        )
        println(warns)
        e.channel.sendMessageEmbeds(embedBuilder.build()).queue()
    }
}