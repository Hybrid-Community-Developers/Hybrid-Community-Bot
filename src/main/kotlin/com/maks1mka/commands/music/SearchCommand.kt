package com.maks1mka.commands.music

import com.maks1mka.commands.BaseCommand
import com.maks1mka.commands.music.lavaplayer.GuildMusicManager
import com.maks1mka.utils.isBotNotInVoice
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class SearchCommand: BaseCommand() {
    override val name: String = "Search"
    override val aliases: List<String> = listOf("search", "searchmusic")
    override val desc: String = "Поиск музыки"
    override val category: String = "Музыка"
    override val usage: String = "<title>"
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if(isBotNotInVoice(e, false)) JoinCommand().onExecute(e, name, args)

        if (args.isEmpty()) {
            e.channel.sendMessage("Ты не указал какую песню искать!").queue()
            return
        }

        val audio = e.guild.audioManager
        val manager: GuildMusicManager = PlayCommand.getMusicManager(e.guild)
        PlayCommand.currentChannel = e.channel
        PlayCommand.currentAuthor = e.member!!
        PlayCommand.currentGuild = e.guild

        val arg: String = args.joinToString(" ")
        audio.sendingHandler = manager.sendHandler
        PlayCommand.loadAndPlay(manager, e.channel, arg, true)
        return
    }
}