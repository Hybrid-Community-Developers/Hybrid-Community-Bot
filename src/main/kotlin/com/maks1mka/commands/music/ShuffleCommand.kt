package com.maks1mka.commands.music

import com.maks1mka.commands.BaseCommand
import com.maks1mka.commands.music.lavaplayer.GuildMusicManager
import com.maks1mka.utils.isBotNotInVoice
import com.maks1mka.utils.isUserNotInVoice
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class ShuffleCommand: BaseCommand() {
    override val name: String = "Shuffle"
    override val aliases: List<String> = listOf("shuffle", "shuf", "sh")
    override val desc: String = "Перемешка треков в очереди"
    override val category: String = "Музыка"
    override val usage: String = ""
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if(isUserNotInVoice(e)) return
        if(isBotNotInVoice(e, true)) return


        val manager: GuildMusicManager = PlayCommand.getMusicManager(e.guild)

        e.channel.sendMessage("Очередь перемешана!").queue()
        manager.scheduler.shuffle()

        return
    }
}