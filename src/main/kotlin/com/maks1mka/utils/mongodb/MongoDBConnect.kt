package com.maks1mka.utils.mongodb

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoDatabase


class MongoDBConnect(login: String, password: String, cluster: String, dataBaseName: String) {

    private val login: String
    private val password: String
    private val dataBaseName: String
    private val cluster: String
    private lateinit var database: MongoDatabase
    private lateinit var client: MongoClient
    init {
        this.login = login
        this.password = password
        this.cluster = cluster
        this.dataBaseName = dataBaseName
        this.connect()
    }

    private fun connect() {
        val connectionString = ConnectionString("mongodb+srv://$login:$password@$cluster.ig3hx.mongodb.net/?retryWrites=true&w=majority")
        val settings = MongoClientSettings.builder()
            .applyConnectionString(connectionString)
            .build()
        this.client = MongoClients.create(settings)
        this.database = this.client.getDatabase(this.dataBaseName)
    }

    fun getDataBase(): MongoDatabase {
        return this.database
    }


    fun getClient(): MongoClient {
        return this.client
    }
}
