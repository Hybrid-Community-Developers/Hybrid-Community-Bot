package com.maks1mka.commands.music

import com.maks1mka.commands.BaseCommand
import com.maks1mka.commands.music.lavaplayer.GuildMusicManager
import com.maks1mka.utils.isBotNotInVoice
import com.maks1mka.utils.isUserNotInVoice
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class SkipCommand: BaseCommand() {
    override val name: String = "Skip"
    override val aliases: List<String> = listOf("skip", "sk", "s")
    override val desc: String = "Пропуск трека"
    override val category: String = "Музыка"
    override val usage: String = ""
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if(isUserNotInVoice(e) || isBotNotInVoice(e)) return

        val manager: GuildMusicManager = PlayCommand.getMusicManager(e.guild)
        e.channel.sendMessage("Песня пропущена!").queue()
        manager.scheduler.nextTrack()
        return
    }

}