package com.maks1mka.commands.`fun`

import com.github.tsohr.JSONObject
import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.getTimestamp
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.net.URL
import kotlin.time.Duration.Companion.seconds


class AnimeSceneSearchCommand: BaseCommand() {

    override val name: String = "AnimeSceneSearch"
    override val aliases: List<String> = listOf("anime", "animesearch")
    override val desc: String = "Поиск аниме по картинке или гифке"
    override val category: String = "Фан"
    override val usage: String = "<url|attached image>"

    private var baseUrl: String = "https://api.trace.moe/search?anilistInfo"

    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {
        if(args.isEmpty() && e.message.attachments.isEmpty() && !e.message.attachments[0].isImage) return
        var imageUrl = ""

        if (e.message.attachments.isNotEmpty() && args.isEmpty()) {
            imageUrl = e.message.attachments[0].url
        } else if(args.isNotEmpty()) {
            imageUrl = args[0]
        }


        var result = JSONObject(URL("$baseUrl&url=$imageUrl").readText(Charsets.UTF_8))

        if (result.getString("error") != "") {
            e.channel.sendMessage("Произошла ошибка: ${result.getString("error")}").queue()
            return
        }

        result = result.getJSONArray("result").getJSONObject(0)

        val titleName: String = result.getJSONObject("anilist").getJSONObject("title").getString("romaji")

        val episodeNumber: Int = try {
            result.getInt("episode")
        } catch (e: Exception) {
            0
        }
        val timeStamp: String = getTimestamp(result.getLong("from").seconds.inWholeMilliseconds)

        val embed: MessageEmbed = EmbedBuilder()
            .setTitle("Информация")
            .setColor(e.author.retrieveProfile().complete().accentColor)
            .setDescription("**Название:** $titleName\n**Серия:** $episodeNumber\n**Метка времени:** $timeStamp")
            .setFooter("Запросил ${e.author.name}", e.author.effectiveAvatarUrl)
            .build()

        e.channel.sendMessageEmbeds(embed).queue()
        return

    }

}