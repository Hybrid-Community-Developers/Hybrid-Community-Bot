# Hybrid-Community-Bot


- [ ] Moderation

    - [x] Base Commands
    - [ ] Auto Moderation
    - [ ] Global Anti-Spam System
    - [ ] Punishments templates

- [x] Music

    - [x] YouTube
    - [x] SoundCloud
