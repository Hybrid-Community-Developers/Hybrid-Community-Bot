package com.maks1mka.commands.moderation

import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.BaseEmbedBuilder
import com.maks1mka.utils.checkPerms
import com.maks1mka.utils.classException
import com.maks1mka.utils.fetchMember
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class UnmuteCommand: BaseCommand() {
    override val name: String = "Unmute"
    override val aliases: List<String> = listOf("unmute", "offtimeout", "untimeout", "deltimeout")
    override val desc: String = "Размьют участника сервера"
    override val category: String = "Модерация"
    override val usage: String = "<member>"
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if (checkPerms(e, e.member!!.permissions)) return

        val member: Member? = fetchMember(e, args)
        if(classException(e,member!!.javaClass, "MemberImpl", true)) return
        if (member == e.member) {
            e.channel.sendMessage("Ты не можешь применять эту команду на себе").queue()
            return
        }

        val embedBuilder: EmbedBuilder = BaseEmbedBuilder().create(
            title = "Успешно!",
            desc =  "${member.asMention} размьючен",
            color = e.author.retrieveProfile().complete().accentColor,
            textFooter = "Размутил ${e.author.name}",
            iconFooter = e.author.effectiveAvatarUrl
        )

        e.guild.removeTimeout(member).queue().apply {
            e.channel.sendMessageEmbeds(embedBuilder.build()).queue()
        }

    }
}