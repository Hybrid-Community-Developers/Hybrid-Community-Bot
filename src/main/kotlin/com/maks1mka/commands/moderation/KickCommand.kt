package com.maks1mka.commands.moderation

import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.BaseEmbedBuilder
import com.maks1mka.utils.checkPerms
import com.maks1mka.utils.classException
import com.maks1mka.utils.fetchMember
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class KickCommand: BaseCommand() {
    override val name: String = "Kick"
    override val aliases: List<String> = listOf("kick")
    override val desc: String = "Исключение участника сервера"
    override val category: String = "Модерация"
    override val usage: String = "<member> [reason]"
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if (checkPerms(e, e.member!!.permissions)) return

        val member: Member? = fetchMember(e, args)
        if(classException(e, member!!.javaClass, "MemberImpl", true)) return

        if (member == e.member) {
            e.channel.sendMessage("Ты не можешь выгнать себя же").queue()
            return
        }

        var reason = "Причина не указана"
        if(args.size > 1) {
            reason = args.subList(1, args.size).joinToString(" ")
        }



        val embedBuilder: EmbedBuilder = BaseEmbedBuilder().create(
            title = "Успешно!",
            desc = "${member.asMention} выгнан\n **Причина:** $reason\n**",
            color = e.member?.color,
            textFooter = "Выгнал ${e.author.name}",
            iconFooter = member.effectiveAvatarUrl,
        )


        e.guild.kick(member).queue().apply {
            e.channel.sendMessageEmbeds(embedBuilder.build()).queue()
        }
        return
    }
}