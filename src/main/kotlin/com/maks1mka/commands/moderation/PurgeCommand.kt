package com.maks1mka.commands.moderation

import com.maks1mka.commands.BaseCommand
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class PurgeCommand: BaseCommand() {
    override val name: String = "Purge"
    override val aliases: List<String> = listOf("purge", "prg", "pg")
    override val desc: String = "Очистка сообщений"
    override val category: String = "Модерация"
    override val usage: String = "[count]"

    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {
        var count = 20
        if (args.isNotEmpty()) count = args[0].toInt()
        e.channel.iterableHistory.takeAsync(count).thenAccept(e.channel::purgeMessages).apply {
            e.channel.sendMessage("Очищено $count сообщений").queue()
        }
        return
    }

}