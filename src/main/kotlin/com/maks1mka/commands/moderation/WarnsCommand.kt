package com.maks1mka.commands.moderation

import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.*
import com.maks1mka.utils.mongodb.UsersCollection
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class WarnsCommand: BaseCommand() {
    override val name: String = "Warns"
    override val aliases: List<String> = listOf("warns", "wanrnscount")
    override val desc: String = "Количество имеющихся предупреждений у участника"
    override val category: String = "Модерация"
    override val usage: String = "[member]"
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        val member: Member? = fetchMember(e, args)
        if(classException(e, member!!.javaClass, "MemberImpl", true)) return

        val warns: Int = UsersCollection(CLIENT, DATABASE).getUserById(e.guild.id, member.id)!!.getInteger("warns")

        val embedBuilder: EmbedBuilder = BaseEmbedBuilder().create(
            title = "Количество предпреждений ${member.user.name}",
            desc = "$warns",
            color = e.author.retrieveProfile().complete().accentColor,
            textFooter = "Запросил ${e.author.name}",
            iconFooter = e.author.effectiveAvatarUrl
        )
        e.channel.sendMessageEmbeds(embedBuilder.build()).queue()
    }
}