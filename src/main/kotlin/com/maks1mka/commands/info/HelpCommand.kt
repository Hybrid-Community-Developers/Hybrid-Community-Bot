package com.maks1mka.commands.info

import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.BaseEmbedBuilder
import com.maks1mka.utils.PREFIX
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.util.*


class HelpCommand: BaseCommand() {
    override val name: String = "Help"
    override val aliases: List<String> = listOf("help", "h", "hlp")
    override val desc: String = "Помощь по командам"
    override val category: String = "Информация"
    override val usage: String = "[command]"

    private val commands: TreeMap<String, BaseCommand> = TreeMap()


    fun registerCommands(commandsList: List<BaseCommand>, builder: JDABuilder): JDABuilder {
        commandsList.forEach {
            cmd -> commands[cmd.name] = cmd
            builder.addEventListeners(cmd)
        }
        return builder
    }

    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {
        if(args.isEmpty()) {
            val categories: ArrayList<String> = arrayListOf()
            commands.values.forEach {
                val category: String = it.category
                if (!categories.contains(category)) categories.add(category)
            }

            var embedBuilder: EmbedBuilder = BaseEmbedBuilder().create(
                title = "Помощь по командам",
                desc = "**Префикс:** `$PREFIX`\n**Справка по конкретной команде:** ${PREFIX}help [command]",
                color = e.author.retrieveProfile().complete().accentColor,
                textFooter = "Запросил ${e.author.name}",
                iconFooter = e.author.effectiveAvatarUrl
            ).setThumbnail(e.jda.selfUser.effectiveAvatarUrl)
            categories.forEach { category ->
                var commandsCategory = StringBuilder()
                var commandsCount = 0
                commands.values.forEach { cmd: BaseCommand ->
                    if (cmd.category == category) {
                        if (cmd.name == "Action") {
                            commandsCount = cmd.aliases.size
                            cmd.aliases.forEach { alias ->
                                commandsCategory.append("`").append(alias).append("`, ")
                            }
                        } else {
                            commandsCount += 1
                            commandsCategory.append("`").append(cmd.aliases[0]).append("`, ")
                        }
                    }
                }
                commandsCategory = StringBuilder(commandsCategory.substring(0, commandsCategory.length - 2))
                embedBuilder = embedBuilder.addField("$category ($commandsCount)", commandsCategory.toString(), false)
            }
            e.channel.sendMessageEmbeds(embedBuilder.build()).queue()
            return
        }

        commands.values.forEach { cmd: BaseCommand ->
            if(cmd.aliases.contains(args[0])) {
                val usage = "$PREFIX${args[0]} ${cmd.usage}"
                val desc: String = cmd.desc.ifEmpty { "_Отсутствует_" }
                var aliases = "_Отсутствуют_"
                var title: String = args[0]
                if(cmd.name != "Action") {
                    title = cmd.name
                    if(cmd.aliases.size > 1) aliases = "`${cmd.aliases.joinToString(", ")}`"

                }
                val embedBuilder: EmbedBuilder = BaseEmbedBuilder().create(
                    title = "Справка о команде ${title.replaceFirstChar { it.titlecase(Locale.getDefault())}}",
                    color =  e.author.retrieveProfile().complete().accentColor,
                    textFooter = "Запросил ${e.author.name}",
                    iconFooter = e.author.effectiveAvatarUrl,)
                    .addField("Описание", desc, false)
                    .addField("Псевдонимы", aliases, false)
                    .addField("Использование", "`$usage`", false)
                    .setThumbnail(e.jda.selfUser.effectiveAvatarUrl)

                e.channel.sendMessageEmbeds(embedBuilder.build()).queue()
                return
            }
        }
    }
}