package com.maks1mka.commands.music

import com.maks1mka.commands.BaseCommand
import com.maks1mka.commands.music.lavaplayer.GuildMusicManager
import com.maks1mka.utils.getTimestamp
import com.maks1mka.utils.isBotNotInVoice
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class NowPlayingCommand: BaseCommand() {
    override val name: String = "NowPlaying"
    override val aliases: List<String> = listOf("nowplaying", "now", "n", "current")
    override val desc: String = "Посмотр проигрываемого трека"
    override val category: String = "Музыка"
    override val usage: String = ""
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if(isBotNotInVoice(e, true)) return

        val manager: GuildMusicManager = PlayCommand.getMusicManager(e.guild)
        if (manager.scheduler.getQueue().isEmpty() && manager.player.playingTrack == null) {
            e.channel.sendMessage("Сейчас ничего не проигрывается!").queue()
            return
        }
        val track: AudioTrack = manager.player.playingTrack

        val currentAuthor = e.member
        val title: String = track.info.title
        val uri: String = track.info.uri
        val position: String = getTimestamp(track.position)
        val duration: String = getTimestamp(track.duration)
        assert(currentAuthor != null)
        val npEmbed = EmbedBuilder()
            .setTitle("Проигрывается")
            .setDescription("**$title**")
            .addField("Время", "$position/$duration", true)
            .addField("Ссылка", uri, true)
            .setColor(currentAuthor!!.user.retrieveProfile().complete().accentColor)
            .setFooter(
                "Запросил " + if (currentAuthor.nickname.isNullOrEmpty()) currentAuthor.user.name else currentAuthor.nickname,
                currentAuthor.user.avatarUrl
            )

        e.channel.sendMessageEmbeds(npEmbed.build()).queue()

    }
}